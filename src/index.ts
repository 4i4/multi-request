import axios, {
  AxiosError,
  AxiosRequestConfig,
  AxiosRequestHeaders,
  AxiosResponse,
  RawAxiosResponseHeaders
} from "axios";

export interface MultiRequestConfig extends AxiosRequestConfig {
  id?: string;
  weight?: number;
  onSuccess?: (_response: AxiosResponse, _requestId?: string) => void;
  onError?: (_error: AxiosError) => void;
  subrequest?: (
    _response: AxiosResponse,
    _requestId?: string
  ) => MultiRequestConfig | MultiRequestConfig[];
}

export interface ResponseCollection<T = any, D = any> {
  [id: string]: AxiosResponse<T, D>;
}

export interface ResponseDataCollection {
  [id: string]: any;
}

export interface ResponseHeadersCollection {
  [id: string]: RawAxiosResponseHeaders;
}

interface Options {
  baseURL: string;
  headers?: AxiosRequestHeaders;
}
type InitOptions = Options | string;

/**
 * @class MultiRequest
 */
class MultiRequest {
  _baseURL?: string;
  _headers?: AxiosRequestHeaders;
  /** @member {MultiRequestConfig[]} */
  _requests: MultiRequestConfig[] = [];
  _response: ResponseCollection = {};
  _weights: { [id: string]: number } = {};

  constructor(options?: InitOptions) {
    if (typeof options === "string") {
      /** @member {string} */
      this._baseURL = options;
    } else if (typeof options === "object") {
      /** @member {string} */
      this._baseURL = options.baseURL;
      /** @member {AxiosRequestHeaders} */
      this._headers = options.headers;
    }
  }

  /**
   * Add a request.
   *
   * @param {MultiRequestConfig} request
   *   The Multi-request object.
   */
  addRequest(request: MultiRequestConfig) {
    if (!request) return;
    this._requests.push(request);
  }

  /**
   * Add response data.
   *
   * @param {string} id
   *   The request ID.
   * @param {AxiosResponse<any, any>} data
   *   The response data.
   *
   * @private
   */
  private addResponse(id: string, data: AxiosResponse<any, any>) {
    this._response[id] = data;
  }

  /**
   * Check if path is absolute.
   *
   * @param {string} path
   *   The path to be checked.
   *
   * @private
   */
  private isPathAbsolute(path?: string): boolean {
    return /^(?:[a-z]+:\/\/)/.test(path ?? "");
  }

  /**
   * Build Request Promise for each request.
   *
   * @param {MultiRequestConfig[]} requests
   *   The array with all requests.
   * @param {string} parent
   *   The parent request ID.
   *
   * @private
   */
  private buildRequests(
    requests: MultiRequestConfig[],
    parent = ""
  ): Promise<any>[] {
    return requests
      .filter(req => req)
      .map((request, index) => {
        const {
          id,
          subrequest,
          headers,
          baseURL,
          url,
          weight,
          onSuccess,
          onError,
          ...config
        } = request;
        const requestId =
          id || (parent && parent + "-" + index) || index.toString();

        this._weights[requestId] = weight ?? index;

        const requestConfig: AxiosRequestConfig = { ...config, url };

        if (this.isPathAbsolute(url)) {
          requestConfig.baseURL = undefined;
        } else {
          requestConfig.baseURL = baseURL ?? this._baseURL;
        }

        requestConfig.headers = {
          ...(this._headers ?? {}),
          ...(headers ?? {})
        };

        return axios(requestConfig)
          .then(async response => {
            this.addResponse(requestId, response);

            if (typeof onSuccess === "function") {
              onSuccess(response, requestId);
            }

            if (typeof subrequest === "function") {
              return subrequest(response, requestId);
            }
            return;
          })
          .then(subrequests => {
            if (!subrequests) return;
            if (Array.isArray(subrequests) && subrequests.length < 1) return;
            if (!Array.isArray(subrequests)) subrequests = [subrequests];
            const requests = this.buildRequests(subrequests, requestId);
            return axios.all(requests);
          })
          .catch(error => {
            if (typeof onError === "function") {
              onError(error);
            }
            this.addResponse(requestId, error);
          });
      });
  }

  /**
   * Get Response Headers for particular request.
   *
   * @param {string} requestId
   *   The request ID.
   */
  getResponseHeaders(
    requestId?: string | RegExp
  ): RawAxiosResponseHeaders | ResponseHeadersCollection | undefined {
    if (typeof requestId === "undefined") {
      return Object.keys(this._response).reduce((output, requestId) => {
        output[requestId] = this._response[requestId]?.headers;
        return output;
      }, {} as ResponseHeadersCollection);
    } else if (typeof requestId === "string") {
      return this._response?.[requestId]?.headers;
    } else {
      return Object.keys(this._response)
        .filter(key => requestId.test(key))
        .reduce((output, requestId) => {
          output[requestId] = this._response[requestId]?.headers;
          return output;
        }, {} as ResponseHeadersCollection);
    }
  }

  /**
   * Get Response Data for particular request.
   *
   * @param {string} requestId
   *   The request ID.
   */
  getResponseData(
    requestId?: string | RegExp
  ): any | ResponseDataCollection | undefined {
    if (typeof requestId === "undefined") {
      return Object.keys(this._response).reduce((output, requestId) => {
        output[requestId] = this._response[requestId]?.data;
        return output;
      }, {} as ResponseDataCollection);
    } else if (typeof requestId === "string") {
      return this._response?.[requestId]?.data;
    } else {
      return Object.keys(this._response)
        .filter(key => requestId.test(key))
        .reduce((output, requestId) => {
          output[requestId] = this._response[requestId]?.data;
          return output;
        }, {} as ResponseDataCollection);
    }
  }

  private sort() {
    const unordered = this._response;
    const ordered: ResponseCollection = {};
    Object.keys(unordered)
      .sort((a, b) => this._weights[a] - this._weights[b])
      .forEach(key => (ordered[key] = unordered[key]));
    this._response = ordered;
  }

  /**
   * Run all requests at once.
   */
  async execute(): Promise<ResponseCollection> {
    const requests = this.buildRequests(this._requests);
    await axios.all(requests);
    this.sort();
    return this._response;
  }
}

export default MultiRequest;
