# MultiRequest

Build multiple requests based on [Axios](https://www.npmjs.com/package/axios) with option for subrequests and execute them as concurrent requests.

## How this package can help you?
This module is using [Axios](https://www.npmjs.com/package/axios) as HTTP client 
and can be considered as helper If you need to execute concurrent requests or/and 
have dependencies between the requests.

## Installing:

Using npm:
```shell
$ npm install @4i4/multi-request
```

Using yarn:
```shell
$ yarn add @4i4/multi-request
```

Using pnpm:
```shell
$ pnpm add @4i4/multi-request
```

## Example
```javascript
import MultiRequest from "@4i4/multi-request";

const baseUrl = "https://swapi.dev"
const client = new MultiRequest(baseUrl);

client.addRequest({
  id: "film",
  url: "/api/films/1",
  subrequest: response => {
    const { characters } = response.data;
    return characters.map((url, index) => ({
      id: `character-${index}`,
      url: url
    }));
  }
});
client.addRequest({
  id: "people",
  url: "/api/people/",
  onSuccess: response => {
    console.log("People:");
    console.table(response.data.results);
  }
});
client.addRequest({
  id: "planets",
  url: "/api/planets/",
  onSuccess: response => {
    console.log("Planets:");
    console.table(response.data.results);
  }
});
client.addRequest({
  id: "species",
  url: "/api/species/",
  onSuccess: response => {
    console.log("Species:");
    console.table(response.data.results);
  }
});

await client.execute();
```
## Creating an instance

**new MultiRequest()**
```javascript
const client = new MultiRequest();
```
**new MultiRequest([baseUrl])**
```javascript
const client = new MultiRequest("https://swapi.dev"); // common baseUrl
```
**new MultiRequest([options])**
```javascript
const client = new MultiRequest({
  baseUrl: "https://swapi.dev", // common baseUrl
  headers: {} // common headers
});
```
## Instance methods
### .addRequest(config)
**Request Config**

The multi-request configuration is extended version of [Axios Request configuration](https://www.npmjs.com/package/axios#request-config).\
The list bellow is what MultiRequest introduce. For all other properties and methods please check [here](https://www.npmjs.com/package/axios#request-config).

| **Property** | **Type**   | **Optional** | **Description**                                                                                                                                              |
|--------------|------------|--------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`         | `string`   | Yes          | `id` is the request ID that can be used to identify the response for this request. If one is not provided it will be automatically created.                  |
| `weight`     | `number`   | Yes          | It will be used to sort the responses. The default value is `0`                                                                                              | 
| `subrequest` | `function` | Yes          | The `subrequest` method receives two parameters - `response` and `requestId`. It should return new request configuration or array of request configurations. |
| `onSuccess`  | `function` | Yes          | The `onSuccess` method receives two parameters - `response` and `requestId`.                                                                                 |
| `onError`    | `function` | Yes          | It's receiving an error object.                                                                                                                              |

### .execute()
Execute all requests and returns the responses sorted by the `weight` property.

### .getResponseHeaders([requestId])

| **Request ID** | **Description**                                  |
|----------------|--------------------------------------------------|
| `null`         | Returns the response headers from all requests.  |
| `string`       | Returns the response headers for single request. |
| `RegExp`       | Returns the response headers for each match.     |

### .getResponseData([requestId])

| **Request ID** | **Description**                               |
|----------------|-----------------------------------------------|
| `null`         | Returns the response data from all requests.  |
| `string`       | Returns the response data for single request. |
| `RegExp`       | Returns the response data for each match.     |
